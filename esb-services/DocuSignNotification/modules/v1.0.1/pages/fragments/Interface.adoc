[cols=",", width=50]
|===

|*Transport Protocol*|HTTPS
|*Transport Security*|Two-way SSL
|*Message Protocol*|SOAP
|===

== Artifacts
* xref:attachment$CetinIntegrationMessage.xsd[CetinIntegrationMessage.xsd,window=_blank]

* xref:attachment$DocuSignManagement.wsdl[DocuSignManagement.wsdl,window=_blank]

* xref:attachment$DocuSignManagement.xsd[DocuSignManagement.xsd,window=_blank]

* xref:attachment$DocusignTestDocument.pdf[DocusignTestDocument.pdf,window=_blank]

== OperationList
[cols=3, options="header"]
|===

|*Operation*|*Request*|*Response*
|createConsoleView|xref:attachment$svg/CreateConsoleViewRequest.svg[CreateConsoleViewRequest,window=_blank]|xref:attachment$svg/CreateConsoleViewResponse.svg[CreateConsoleViewResponse,window=_blank]
|createEditView|xref:attachment$svg/CreateEditViewRequest.svg[CreateEditViewRequest,window=_blank]|xref:attachment$svg/CreateEditViewResponse.svg[CreateEditViewResponse,window=_blank]
|createEnvelope|xref:attachment$svg/CreateEnvelopeRequest.svg[CreateEnvelopeRequest,window=_blank]|xref:attachment$svg/CreateEnvelopeResponse.svg[CreateEnvelopeResponse,window=_blank]
|createRecipientView|xref:attachment$svg/CreateRecipientViewRequest.svg[CreateRecipientViewRequest,window=_blank]|xref:attachment$svg/CreateRecipientViewResponse.svg[CreateRecipientViewResponse,window=_blank]
|createSenderView|xref:attachment$svg/CreateSenderViewRequest.svg[CreateSenderViewRequest,window=_blank]|xref:attachment$svg/CreateSenderViewResponse.svg[CreateSenderViewResponse,window=_blank]
|getCustomFields|xref:attachment$svg/GetCustomFieldsRequest.svg[GetCustomFieldsRequest,window=_blank]|xref:attachment$svg/GetCustomFieldsResponse.svg[GetCustomFieldsResponse,window=_blank]
|getDocument|xref:attachment$svg/GetDocumentRequest.svg[GetDocumentRequest,window=_blank]|xref:attachment$svg/GetDocumentResponse.svg[GetDocumentResponse,window=_blank]
|getDocuments|xref:attachment$svg/GetDocumentsRequest.svg[GetDocumentsRequest,window=_blank]|xref:attachment$svg/GetDocumentsResponse.svg[GetDocumentsResponse,window=_blank]
|getEnvelope|xref:attachment$svg/GetEnvelopeRequest.svg[GetEnvelopeRequest,window=_blank]|xref:attachment$svg/GetEnvelopeResponse.svg[GetEnvelopeResponse,window=_blank]
|getRecipients|xref:attachment$svg/GetRecipientsRequest.svg[GetRecipientsRequest,window=_blank]|xref:attachment$svg/GetRecipientsResponse.svg[GetRecipientsResponse,window=_blank]
|updateCustomFields|xref:attachment$svg/UpdateCustomFieldsRequest.svg[UpdateCustomFieldsRequest,window=_blank]|xref:attachment$svg/UpdateCustomFieldsResponse.svg[UpdateCustomFieldsResponse,window=_blank]
|===
