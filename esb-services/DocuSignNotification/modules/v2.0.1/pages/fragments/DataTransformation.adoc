:path: fragments/
:imagesdir: images/

ifdef::rootpath[]
:imagesdir: {rootpath}{path}{imagesdir}
endif::rootpath[]

ifndef::rootpath[]
:rootpath: ./../../
endif::rootpath[]
*CreateEnvelopeRequest*

[width="100%",cols="22%,40%,9%,29%",]
|===
|*Source* |*Target* |*Mapping* |

|requestBody (struktura) |requestBody (struktura) | |

| |accountId |konstanta ID účtu kdocusign, bude dodáno dle licence |

|envelope (struktura) | | |

|subject |envelope\emailSubject | |

|message |envelope\emailBlurb | |

|recipients (struktura) | | |

|recipient (struktura) | | |

|email |envelope\recipients[x]\signers\email | |

|phoneNumber |envelope\recopients[x]\signers\customFields[0] |do custom
fieldu k podepisujícímu ukládáme telefonní číslo (pokud přišlo a
generovali jsme accessCode). Pozor, nejedná se o sekci customFields na
envelope |

|fullName |envelope\recipients[x]\signers\name | |

|order |envelope\recipients[x]\signers\routingOrder | |

|recipientId |envelope\recipients[x]\signers\recipientId | |

|signatureProviders a|
envelope\recipients[x]\signers\recipientSignatureProviders

\signatureProviderName

| |

|generateAccessCode |envelope\recipients[x]\signers\accessCode |když je
vsource true, generujeme unikátní alfanumerickou sekvenci 6 znaků (case
sensitive), když je false, neposíláme do výstupu |

| a|
envelope\recipients[x]\signers\recipientSignatureProviders\

signatureProviderOptions\oneTimePassword

a|
{empty}1) generateAccessCode = true &

universalsignaturepen_opentrust_hash_tsp

=> vytváříme accessCode i oneTimePassword

{empty}2) generateAccessCode = false &

universalsignaturepen_opentrust_hash_tsp

=> vytváříme oneTimePassword

{empty}3) generateAccessCode = true &

=> vytváříme accessCode

v ostatních případech neděláme nic

|

|tabSignature (struktura) | | |

|tab (struktura) | | |

|recipientId
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\recipientId | |

|anchorString
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\anchorString | |

|anchorUnits
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\anchorUnits | |

|anchorXOffset
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\anchorXOffset | |

|anchorYOffset
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\anchorYOffset | |

|name |envelope\recipients[x]\signers\tabs\signHereTabs\tabs\name | |

|tabLabel
|envelope\recipients[x]\signers\tabs\signHereTabs\tabs\tabLabel | |

|tabSignatureDate (struktura) | | |

|tab (struktura) | | |

|recipientId
|envelope\recipients[x]\signers\tabs\dateTabs\tabs\recipientId | |

|anchorString
|envelope\recipients[x]\signers\tabs\dateTabs\tabs\anchorString | |

|anchorUnits
|envelope\recipients[x]\signers\tabs\dateTabs\tabs\anchorUnits | |

|anchorXOffset
|envelope\recipients[x]\signers\tabs\dateTabs\tabs\anchorXOffset | |

|anchorYOffset
|envelope\recipients[x]\signers\tabs\dateTabs\tabs\anchorYOffset | |

|fontSize |envelope\recipients[x]\signers\tabs\dateTabs\tabs\fontSize |
|

|name |envelope\recipients[x]\signers\tabs\dateTabs\tabs\name | |

|tabLabel |envelope\recipients[x]\signers\tabs\dateTabs\tabs\tabLabel |
|

|tabSignatureName (struktura) | | |

|tab (struktura) | | |

|recipientId
|envelope\recipients\signers\tabs\fullNameTabs\tabs\recipientId | |

|anchorString
|envelope\recipients\signers\tabs\fullNameTabs\tabs\anchorString | |

|anchorUnits
|envelope\recipients\signers\tabs\fullNameTabs\tabs\anchorUnits | |

|anchorXOffset
|envelope\recipients\signers\tabs\fullNameTabs\tabs\anchorXOffset | |

|anchorYOffset
|envelope\recipients\signers\tabs\fullNameTabs\tabs\anchorYOffset | |

|fontSize |envelope\recipients\signers\tabs\fullNameTabs\tabs\fontSize |
|

|name |envelope\recipients\signers\tabs\fullNameTabs\tabs\name | |

|tabLabel |envelope\recipients\signers\tabs\fullNameTabs\tabs\tabLabel |
|

|deliveryMethod |envelope\recipients\signers\deliveryMethod |pokud není
vyplněn, dáváme konstantu=email |

|carbonCopy (struktura) | | |

|email |envelope\recipients\carbonCopies\email | |

|fullName |envelope\recipients\carbonCopies\name | |

|order |envelope\recipients\carbonCopies\routingOrder | |

|carbonCopyId |envelope\recipients\carbonCopies\recipientId | |

|customFields (struktura) | | |

|customField (struktura) | | |

|fieldId |envelope\customFields\textCustomFields\customField\fieldId | |

|name |envelope\customFields\textCustomFields\customField\name | |

|show |envelope\customFields\textCustomFields\customField\show | |

|required |envelope\customFields\textCustomFields\customField\required |
|

|value |envelope\customFields\textCustomFields\customField\value | |

|configurationType
|envelope\customFields\textCustomFields\customField\configurationType |
|

|
|envelope\customFields\textCustomFields\customField[name="tSignerId"]\Value
|Pro kažrou envelope bude ESB zakládat nový textový custom field kde
name=**currentSignerId**, **value**= prázdný string, **show**=false,
**required**=false |

|documents | | |

|document | | |

|name |envelope\documents\document\name | |

|documentId |envelope\documents\document\documentId | |

|fullContentDocument (struktura)(choice) | | |

|fileExtension |envelope\documents\document\fileExtension |(vyplňuje
konzument z enumerace, mapování je 1:1) |

|content |envelope\documents\document\documentBase64 |mapuje se přímo
1:1 |

|urlDocument (struktura)(choice) | | |

| |envelope\documents\document\fileExtension a|
zkontrolovat mime type, pokud je:

application/pdf - pouzijeme konstantu=pdf

application/docx - použijeme konstantu docx

v ostatních případech vracíme chybu, že jiný formát než pdf a docx není
podporován

|

|documentUrl |envelope\documents\document\documentBase64 |mapuje base64
encoded content vyzvednutý z url definované v documentUrl |

|dmsLiveLinkDocument (struktura)(choice) | | |

| |envelope\documents\document\fileExtension | |konstanta=pdf

|repositoryId | | |

|repositoryDocumentId |envelope\documents\document\documentBase64 |
|mapuje se base64 encoded content vyzvednutý z livelink DMS

|status |envelope\status | |
|===

*CreateEnvelopeResponse*

*HTTP 200*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\envelopeId |responseBody\envelopeId |
|responseBody\status |responseBody\status |
|responseBody\errorDetails\errorCode |responseBody\error\errorCode |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\errorCode |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*GetEnvelopeRequest*

/accounts/\{accountId}/envelopes/\{envelopeId}?include=\{include}

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody\include |requestBody\include |v xsd je jako enum list, tj.
legální zápis jsou hodnoty z enumerace oddělené mezerou. Pro APIM je
nutné provést replace mezery za čárku, tj když mám např. "custom_fields
documents", transformujeme na "custom_fields,documents"
|===

*GetEnvelopeResponse*

*HTTP 200*

[width="100%",cols="41%,45%,14%",]
|===
|*Source* |*Target* |*Mapping*

|responseBody\envelope\allowMarkup |responseBody\envelope\allowMarkup |

|responseBody\envelope\autoNavigation
|responseBody\envelope\autoNavigation |

|responseBody\envelope\brandId |responseBody\envelope\brandId |

|responseBody\envelope\certifikateUri
|responseBody\envelope\certifikateUri |

|responseBody\envelope\createdDateTime
|responseBody\envelope\createdDateTime |

|responseBody\envelope\customFieldsUri
|responseBody\envelope\customFieldsUri |

a|
responseBody\envelope\customFields

\textCustomFields\customField\fieldId

a|
responseBody\envelope

\customFields\customField\fieldId

|

a|
responseBody\envelope\customFields

\textCustomFields\customField\name

a|
responseBody\envelope

\customFields\customField\name

|

a|
responseBody\envelope\customFields

\textCustomFields\customField\show

a|
responseBody\envelope

\customFields\customField\show

|

a|
responseBody\envelope\customFields

\textCustomFields\customField\required

a|
responseBody\envelope

\customFields\customField\required

|

a|
responseBody\envelope\customFields

\textCustomFields\customField\value

a|
responseBody\envelope

\customFields\customField\value

|

a|
responseBody\envelope\customFields

\textCustomFields\customField\configurationType

a|
responseBody\envelope\customFields

\customField\configurationType

|

|responseBody\envelope\documentsCombinedUri
|responseBody\envelope\documentsCombinedUri |

|responseBody\envelope\documentsUri |responseBody\envelope\documentsUri
|

a|
responseBody\envelope\envelopeDocuments

\document\name

|responseBody\envelope\envelopeDocuments\name |

a|
responseBody\envelope\envelopeDocuments

\document\order

|responseBody\envelope\envelopeDocuments\order |

a|
responseBody\envelope\envelopeDocuments

\document\documentId

|responseBody\envelope\envelopeDocuments\documentId |

a|
responseBody\envelope\envelopeDocuments

\document\documentBase64

|responseBody\envelope\envelopeDocuments\content |

|responseBody\envelope\emailBlurb |responseBody\envelope\emailMessage |

|responseBody\envelope\emailSubject |responseBody\envelope\emailSubject
|

|responseBody\envelope\enableWetSign
|responseBody\envelope\enableWetSign |

|responseBody\envelope\envelopeId |responseBody\envelope\envelopeId |

|responseBody\envelope\envelopeIdStamping
|responseBody\envelope\envelopeIdStamping |

|responseBody\envelope\envelopeUri |responseBody\envelope\envelopeUri |

|responseBody\envelope\initialSentDateTime
|responseBody\envelope\initialSentDateTime |

|responseBody\envelope\isSignatureProviderEnvelope
|responseBody\envelope\isSignatureProviderEnvelope |

|responseBody\envelope\lastModifiedDateTime
|responseBody\envelope\lastModifiedDateTime |

|responseBody\envelope\notificationUri
|responseBody\envelope\notificationUri |

|responseBody\envelope\purgeState |responseBody\envelope\purgeState |

|responseBody\envelope\recipientsUri
|responseBody\envelope\recipientsUri |

a|
responseBody\envelope\recipients\

signers\accessCode

|responseBody\envelope\recipients\recipient\accessCode |

a|
responseBody\envelope\recipients\

signers\email

|responseBody\envelope\recipients\recipient\email |

a|
responseBody\envelope\recipients\

signers\phoneNumber

|responseBody\envelope\recipients\recipient\phoneNumber |

a|
responseBody\envelope\recipients\

signers\fullName

|responseBody\envelope\recipients\recipient\fullName |

a|
responseBody\envelope\recipients\

signers\routingOrder

|responseBody\envelope\recipients\recipient\order |

a|
responseBody\envelope\recipients\

signers\recipientId

|responseBody\envelope\recipients\recipient\recipientId |

a|
responseBody\envelope\recipients\signers

\recipientSignatureProviders

\signatureProviderName

a|
responseBody\envelope\recipients\recipient

\signatureProviders

|

a|
responseBody\envelope\recipients\signers

\recipientSignatureProviders

\signatureProviderOptions\oneTimePassword

a|
responseBody\envelope\recipients\recipient

\oneTimePassword

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\recipientId

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorString

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorUnits

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorXOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorYOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\name

|

a|
responseBody\envelope\recipients\signers\tabs

\signHereTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\tabLabel

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\recipientId

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorString

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorUnits

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorXOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorYOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\fontSize

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\fontSize

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\name

|

a|
responseBody\envelope\recipients\signers\tabs

\dateTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\tabLabel

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\recipientId

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorString

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorUnits

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorXOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorYOffset

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\fontSize

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\fontSize

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\name

|

a|
responseBody\envelope\recipients\signers\tabs

\fullNameTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\tabLabel

|

a|
responseBody\envelope\recipients

\carbonCopies\email

|responseBody\envelope\recipients\carbonCopy\email |

a|
responseBody\envelope\recipients

\carbonCopies\fullName

|responseBody\envelope\recipients\carbonCopy\fullName |

a|
responseBody\envelope\recipients

\carbonCopies\routingOrder

|responseBody\envelope\recipients\carbonCopy\order |

a|
responseBody\envelope\recipients

\carbonCopies\recipientId

|responseBody\envelope\recipients\carbonCopy\carbonCopyId |

|responseBody\envelope\sentDateTime |responseBody\envelope\sentDateTime
|

|responseBody\envelope\sender\accountId
|responseBody\envelope\sender\accountId |

|responseBody\envelope\sender\userId
|responseBody\envelope\sender\userId |

|responseBody\envelope\sender\userName
|responseBody\envelope\sender\userName |

|responseBody\envelope\sender\email |responseBody\envelope\sender\email
|

|responseBody\envelope\status |responseBody\envelope\status |

|responseBody\envelope\statusChangedDateTime
|responseBody\envelope\statusChangedDateTime |

|responseBody\envelope\templatesUri |responseBody\envelope\templatesUri
|
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*GetRecipientsRequest*

/accounts/\{accountId}/envelopes/\{envelopeId}/recipients

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |
|===

*GetRecipientsResponse*

*HTTP 200*

[width="99%",cols="41%,41%,18%",]
|===
|*Source* |*Target* |*Mapping*

|responseBody\recipients\signers\accessCode
|responseBody\envelope\recipients\recipient\accessCode |

|responseBody\recipients\signers\email
|responseBody\envelope\recipients\recipient\email |

|responseBody\recipients\signers\phoneNumber
|responseBody\envelope\recipients\recipient\phoneNumber |

|responseBody\recipients\signers\fullName
|responseBody\envelope\recipients\recipient\fullName |

|responseBody\recipients\signers\routingOrder
|responseBody\envelope\recipients\recipient\order |

|responseBody\recipients\signers\recipientId
|responseBody\envelope\recipients\recipient\recipientId |

a|
responseBody\recipients\signers

\recipientSignatureProviders

\signatureProviderName

a|
responseBody\envelope\recipients\recipient

\signatureProviders

|

a|
responseBody\recipients\signers

\recipientSignatureProviders

\signatureProviderOptions\oneTimePassword

a|
responseBody\envelope\recipients\recipient

\oneTimePassword

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\recipientId

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorString

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorUnits

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorXOffset

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\anchorYOffset

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\name

|

a|
responseBody\recipients\signers\tabs

\signHereTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignature\tab\tabLabel

|

a|
responseBody\recipients\signers\tabs

\dateTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\recipientId

|

a|
responseBody\recipients\signers\tabs

\dateTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorString

|

a|
responseBody\recipients\signers\tabs

\dateTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorUnits

|

a|
responseBody\recipients\signers\tabs

\dateTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorXOffset

|

a|
responseBody\recipients\signers\tabs

\dateTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\anchorYOffset

|

a|
responseBody\recipients\signers\tabs

\dateTabs\fontSize

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\fontSize

|

a|
responseBody\recipients\signers\tabs

\dateTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\name

|

a|
responseBody\recipients\signers\tabs

\dateTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignatureDate\tab\tabLabel

|

a|
responseBody\recipients

\signers\tabs\fullNameTabs\recipientId

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\recipientId

|

a|
responseBody\recipientsEnvelopeRecipients

\signers\tabs\fullNameTabs\anchorString

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorString

|

a|
responseBody\recipients

\signers\tabs\fullNameTabs\anchorUnits

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorUnits

|

a|
EnvelopeRecipients\signers\tabs

\fullNameTabs\anchorXOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorXOffset

|

a|
responseBody\recipients\signers\tabs

\fullNameTabs\anchorYOffset

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\anchorYOffset

|

a|
responseBody\recipients\signers\tabs

\fullNameTabs\fontSize

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\fontSize

|

a|
responseBody\recipients\signers\tabs

\fullNameTabs\name

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\name

|

a|
responseBody\recipients\signers\tabs

\fullNameTabs\tabLabel

a|
responseBody\envelope\recipients\recipient

\tabSignatureName\tab\tabLabel

|

|responseBody\recipients\carbonCopies\email
|responseBody\envelope\recipients\carbonCopy\email |

|responseBody\recipients\carbonCopies\fullName
|responseBody\envelope\recipients\carbonCopy\fullName |

|responseBody\recipients\carbonCopies\routingOrder
|responseBody\envelope\recipients\carbonCopy\order |

|responseBody\recipients\carbonCopies\recipientId
|responseBody\envelope\recipients\carbonCopy\carbonCopyId |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*GetDocumentRequest*

/accounts/\{accountId}/envelopes/\{envelopeId}/documents/\{documentId}

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody\documentId |requestBody\documentId |

|requestBody\alternativeRetrieve |requestBody\documentId |v docusign je
možné místo ID dokumentu poslat hodnotu zenumerace, vXSD je documentId a
alternativeRetrieve choice, tj. je vyplněn jeden nebo druhý
|===

*GetDocumentResponse*

*HTTP 200*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\content |responseBody\content |
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*GetDocumentsRequest*

/accounts/\{accountId}/envelopes/\{envelopeId}/documents

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |
|===

*GetDocumentsResponse*

*HTTP 200*

[width="100%",cols="45%,37%,18%",]
|===
|*Source* |*Target* |*Mapping*

|responseBody\documents\document\name
|responseBody\documents\document\name |

|responseBody\documents\document\order
|responseBody\documents\document\order |

|responseBody\documents\document\documentId
|responseBody\documents\document\documentId |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*GetCustomFieldsRequest*

/accounts/\{accountId}/envelopes/\{envelopeId}/custom_fields

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |
|===

*GetCustomFieldsResponse*

*HTTP 200*

[width="100%",cols="48%,43%,9%",]
|===
|*Source* |*Target* |*Mapping*

|responseBody\textCustomFields\customField\fieldId
|responseBody\customFields\customField\fieldId |

|responseBody\textCustomFields\customField\name
|responseBody\customFields\customField\name |

|responseBody\textCustomFields\customField\show
|responseBody\customFields\customField\show |

|responseBody\textCustomFields\customField\required
|responseBody\customFields\customField\required |

|responseBody\textCustomFields\customField\value
|responseBody\customFields\customField\value |

|responseBody\textCustomFields\customField\configurationType
|responseBody\customFields\customField\configurationType |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*UpdateCustomFieldsRequest*

PUT /accounts/\{accountId}/envelopes/\{envelopeId}/custom_fields

[width="100%",cols="38%,44%,18%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

| | |

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody \customFields\customField\fieldId
|requestBody\textCustomFields\customField\fieldId |

|requestBody \customFields\customField\name
|requestBody\textCustomFields\customField\name |

|requestBody \customFields\customField\show
|requestBody\textCustomFields\customField\show |

|requestBody \customFields\customField\required
|requestBody\textCustomFields\customField\required |

|requestBody \customFields\customField\value
|requestBody\textCustomFields\customField\valid |

|requestBody \customFields\customField\configurationType
|requestBody\textCustomFields\customField\configurationType |
|===

*UpdateCustomFieldsResponse*

*HTTP 200*

[width="100%",cols="44%,43%,13%",]
|===
|*Source* |*Target* |*Mapping*

|responseBody\textCustomFields\customField\fieldId
|responseBody\customFields\customField\fieldId |

|responseBody\textCustomFields\customField\name
|responseBody\customFields\customField\name |

|responseBody\textCustomFields\customField\show
|responseBody\customFields\customField\show |

|responseBody\textCustomFields\customField\required
|responseBody\customFields\customField\required |

|responseBody\textCustomFields\customField\value
|responseBody\customFields\customField\value |

|responseBody\textCustomFields\customField\configurationType
|responseBody\customFields\customField\configurationType |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
| |responseBody\envelopeId |nemapuje se
| |responseBody\status |nemapuje se
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*CreateConsoleViewRequest*

POST /accounts/\{accountId}/views/console

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody\returnUrl |requestBody\\returnUrl |
|===

*HTTP 201*

[width="100%",cols="44%,43%,13%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\url |responseBody\url |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*CreateEditViewRequest*

POST /accounts/\{accountId}/envelopes/\{envelopeId}/views/edit

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody\returnUrl |requestBody\returnUrl |
|===

*HTTP 201*

[width="100%",cols="44%,43%,13%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\url |responseBody\url |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===

*CreateRecipientViewRequest*

POST /accounts/\{accountId}/envelopes/\{envelopeId}/views/edit

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*

| |requestBody\accountId |konstanta ID účtu kdocusign, bude dodáno dle
licence

|requestBody\envelopeId |requestBody\envelopeId |

|requestBody\authenticationMethod |requestBody\authenticationMethod |

|requestBody\clientUserId |requestBody\clientUserId |

|requestBody\userName |requestBody\userName |

|requestBody\email |requestBody\email |

|requestBody\returnUrl |requestBody\returnUrl |
|===

*HTTP 201*

[width="100%",cols="44%,43%,13%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\url |responseBody\url |
|===

*HTTP 400*

[width="100%",cols="34%,33%,33%",]
|===
|*Source* |*Target* |*Mapping*
|responseBody\errorDetails\errorCode |responseBody\error\code |
|responseBody\errorDetails\message |responseBody\error\message |
|===
