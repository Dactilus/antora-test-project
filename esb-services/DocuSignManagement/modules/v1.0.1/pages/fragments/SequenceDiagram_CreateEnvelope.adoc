[plantuml, CreateEnvelope, png]
----
@startuml
  "Service Consumer" -> "DocuSignManagement/1.0": «SOAP/HTTPS» CreateEnvelope(CreateEnvelopeRequest)
activate "DocuSignManagement/1.0"

  "DocuSignManagement/1.0" -> "DocuSignManagement/1.0.1": 
activate "DocuSignManagement/1.0.1"

group alt [DocumentSource=FullContentDocument]
  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» UsePayloadAsIs

else DocumentSource=DmsLiveLinkDocument
  "DocuSignManagement/1.0.1" -> "DMSLiveLink_DocuSignDocumentManagement/1.0": «SOAP/HTTPS» GetRequestedDocument(GetRequestedDocumentRequest)
activate "DMSLiveLink_DocuSignDocumentManagement/1.0"

  "DMSLiveLink_DocuSignDocumentManagement/1.0" --> "DocuSignManagement/1.0.1": 
deactivate "DMSLiveLink_DocuSignDocumentManagement/1.0"

group alt [resultCode<>0]
  "DocuSignManagement/1.0.1" --> "DocuSignManagement/1.0": 

note right
Když DMS nevrátí dokuemnt, končíme a vracíme konzumentovi chybu
end note
deactivate "DocuSignManagement/1.0.1"

else resultCode=0
  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» EncodeContentToBase64

end

else DocumentSource=UrlDocument
  "DocuSignManagement/1.0.1" -> "HttpLinkStorage": «HTTPS» httpGet
activate "HttpLinkStorage"

  "HttpLinkStorage" --> "DocuSignManagement/1.0.1": 
deactivate "HttpLinkStorage"

  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» EncodeContentToBase64()

end

  "DocuSignManagement/1.0" --> "Service Consumer": 
deactivate "DocuSignManagement/1.0"

group loop over Signer
group alt [generateAccessCode=true && signatureProviderName==universalsignaturepen_opentrust_hash_tsp]
  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» GenerateAccessCode

  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» InjecAccessCodeToOutputRequest

  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» SetOneTimePassword

note right
oneTimePassword=vygenerovaný accessCode

end note

else generateAccessCode=true && signatureProviderName<>universalsignaturepen_opentrust_hash_tsp
  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» GenerateAccessCode

  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» InjecAccessCodeToOutputRequest

end

end

  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» TransformToOutputXmlFormat

  "DocuSignManagement/1.0.1" -> "docusign-esignature-management/1.0": «SOAP/HTTPS» CreateEnvelope(CreateEnvelopeRequest)
activate "docusign-esignature-management/1.0"

  "docusign-esignature-management/1.0" --> "DocuSignManagement/1.0.1": 
deactivate "docusign-esignature-management/1.0"

group alt [response is OK]
group loop over signer
  "DocuSignManagement/1.0.1" -> "DocuSignManagement/1.0.1": «LOCAL» GenerateSmsRequestWithOneTimePassword

  "DocuSignManagement/1.0.1" -> "NotificationMessageHandling/1.4": «SOAP/HTTPS» sendSms(SendSmsRequest)
activate "NotificationMessageHandling/1.4"

  "NotificationMessageHandling/1.4" --> "DocuSignManagement/1.0.1": 
deactivate "NotificationMessageHandling/1.4"

end

end

  "DocuSignManagement/1.0.1" --> "DocuSignManagement/1.0": 
deactivate "DocuSignManagement/1.0.1"

  "DocuSignManagement/1.0" --> "Service Consumer": 
deactivate "DocuSignManagement/1.0"

@enduml
----
