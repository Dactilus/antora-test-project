:path: fragments/
:imagesdir: images/

ifdef::rootpath[]
:imagesdir: {rootpath}{path}{imagesdir}
endif::rootpath[]

ifndef::rootpath[]
:rootpath: ./../../
endif::rootpath[]
*Produkční prostředí*

Z důvodu přenosu citlivých dat a GDPR je na službě vypnut auditlog,
ukládá se pouze access log. (V rámci pilotu bude zapnut i auditlog)

*Ostatní prostředí*

Používá se standardní logování.
